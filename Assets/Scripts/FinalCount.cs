﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class FinalCount : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI textRight;
    [SerializeField] TextMeshProUGUI textWrong;
    // Start is called before the first frame update
    void Start()
    {
        Quiz quiz = FindObjectOfType<Quiz>();
        textRight.text = " Правильных ответов: " + quiz.Right.ToString() +"";
        textWrong.text = " Неправильных ответов: " + quiz.Wrong.ToString() + "";
        
    }

  

    // Update is called once per frame
    void Update()
    {
   
    }
}
