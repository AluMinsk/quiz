﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Quiz : MonoBehaviour
{
   // [SerializeField] string gameTitle;
    //[SerializeField] TextMeshProUGUI titleComponent;
    [SerializeField] TextMeshProUGUI var1;
    [SerializeField] TextMeshProUGUI var2;
    [SerializeField] TextMeshProUGUI var3;
    [SerializeField] TextMeshProUGUI var4;
    [SerializeField] State startState;
    [SerializeField] State nextState;
    [SerializeField] Image animalImage;
    [SerializeField] public int Right;
    [SerializeField] public int Wrong;
    [SerializeField] TextMeshProUGUI textRight;
    [SerializeField] TextMeshProUGUI textWrong;


    State currentState;
    // Start is called before the first frame update


    void Start()
    {
        currentState = startState;
        DontDestroyOnLoad(gameObject);
    }


    public void LoadNextState()
    {
        currentState = currentState.GetNextStates();
        if (currentState == null)
        {
            SceneManager.LoadScene(2);
        }
    }


    public void PressVar1()
    {
        if (currentState.Right == 1)
        {
            Right++;
        }
        else
        {
            Wrong++;
        }

        showResult();
        GameOver();
    }


    public void PressVar2()
    {
        if (currentState.Right == 2)
        {
            Right++;
        }
        else
        {
            Wrong++;
        }
        showResult();
        GameOver();
    }


    public void PressVar3()
    {
        if (currentState.Right == 3)
        {
            Right++;
        }
        else
        {
            Wrong++;
        }
        showResult();
        GameOver();
    }


    public void PressVar4()
    {
        if (currentState.Right == 4)
        {
            Right++;
        }
        else
        {
            Wrong++;
        }
        showResult();
        GameOver();
    }


    public void showResult()
    {
        if (Right > 0)
        {
            textRight.text = "Ура " + Right.ToString() + " правильно!";
        }
        if (Wrong > 0)
        {
            textWrong.text = "Жаль, но " + Wrong.ToString() + " не правильно!";
        }
        LoadNextState();
    }


    public void GameOver()
    {
        if (Wrong > 1)
        {
            SceneManager.LoadScene(3);
        }
    }

    // Update is called once per frame
    void Update()
    {
        var1.text = currentState.GetVar1();
        var2.text = currentState.GetVar2();
        var3.text = currentState.GetVar3();
        var4.text = currentState.GetVar4();
        animalImage.sprite = currentState.GetAnimal();
       
    }
}
