﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
   
    [SerializeField] Sprite animal;
    [SerializeField] string Var1;
    [SerializeField] string Var2;
    [SerializeField] string Var3;
    [SerializeField] string Var4;
    [SerializeField] State nextState;
    [SerializeField] public int Right;

    public string GetVar1()
    {       
        return Var1;
    }

    public string GetVar2()
    {
        return Var2;
    }

    public string GetVar3()
    {
        return Var3;
    }

    public string GetVar4()
    {
        return Var4;
    }

    public Sprite GetAnimal()
    {
        return animal;
    }

    
    public State GetNextStates()
    {
        return nextState;
    }


   
}
